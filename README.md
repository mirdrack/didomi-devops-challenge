# Didomi DevOps Challenge
This repository contains the Clemente Estrada DevOps challenge for Didomi

## General assumptions
In order to focus on the general solution and not going in details that could
add unnecessary complexity for the challenge, there will be some assumptions:

* There is already setup an infrastructure to handle the terraform state storage
and locking.
* One of the lambdas will interact with an S3 bucket.
* There will be just one public subnet and two private subnets.
* Project will be deployed from a configured environment with the necesary
tools installed such as serverless installed globally and terraform installed.

## About this repository structure and the workflow
This repository was built using serverless to generate the basic scaffolding.

To make it self-deployable an infrastructure directory was added to contain a
terraform repository to handle all the cloud resources.

Only the cloud resources related to the custom domain for the application will
be deployed with serverless framework using the plugin serverless domain manager
The application will be deployed using serverless framework tools and all the
configuration parameters will be queried from the aws parameter store based on
the stage.

The only configuration entry point will be the terraform.<env>.tfvars provided
for the terraform project.

Documentation for the infrastructure is on `infrastructure` directory.

## Initialize for development
To initalize the application install node dependencies running:
```
npm install
```

## Deploy to the cloud
To deploy the application to the cloud run the next command:
```
serverless deploy --stage <stg|prod>
```

## Configure the custom domain
Once dependencies are installed, cloud infrastructure has been deployed and
the project has been deployed, a custom domain can be configured through
the framework.
It will retrieve the values from the aws parameter store and configure the
custom domain running the next command:
```
serverless create_domain
```
**Notice** that application must be already deployed to configure the domain.
