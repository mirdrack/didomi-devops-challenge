'use strict';

/**
 * This function will get the stage from an environment variable and return
 * an http response with that value.
 * @param  event Http that triggers the lambda
 * @return Http response that will bee handled by the API Gate way
 */
module.exports.healthcheck = async (event) => {
  let response = {
    message: 'Hello from ' + process.env.STAGE,
    input: event,
  };
  return {
    statusCode: 200,
    body: JSON.stringify(response),
  };
};

/**
 * This function will initialize a connection to the database and perform a
 * generic query.
 * @param  event Http that triggers the lambda
 * @return Http response that will bee handled by the API Gate way
 */
module.exports.dbquery = async (event) => {
  const { Pool, Client } = require("pg");

  const credentials = {
    user: process.env.DATABASE_USER,
    host: process.env.DATABASE_ENDPOINT,
    database: process.env.DATABASE_NAME,
    password: process.env.DATABASE_PASSWORD,
    port: 5432,
  };

  const pool = new Pool(credentials);
  pool.query('SELECT NOW()', (err, res) => {
    console.log(err, res)
    pool.end()
  });


  let response = {
    message: 'DB query',
    input: event,
  };
  return {
    statusCode: 200,
    body: JSON.stringify(response),
  };
};
