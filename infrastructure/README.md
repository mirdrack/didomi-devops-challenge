# Didomi DevOps Challenge Infrastructure
This will contains the terraform project with the infrastructure needed for the
challenge.


## About the infrastructure
The project will have the next assumptions:
* Infrastructure will be deployed to aws.
* Terraform will use access keys for authentication.
* It will use an aws backend to host the terraform state and a dynamoDB table
to lock the state.
* Lambdas will need internet access to hit an external api.
* A hosted zone was created manually to handle the custom domain.
* A Gitlab runner will be provided to create the custom gitlab runners.


For an enterprise environment it would be better using the using terraform cloud
to manage the infrastructure due to the multiple features such as: state
management, user and permissions management, simplified integration for CI/CD
processes, secrets management and others.

## Infrastructure initialization
Requirements:
* Access keys are capable of deploying all the infrastructure resources.
* `terraform >= v1.0.3`

### Initialization of the Terraform project
1. Configure the `terraform.<stg|prod>.tfvars` with the proper values.
2. Execute `terraform init -var-file terraform.<stg|prod>.tfvars` to initialize the
infrastructure.
3. Execute `terraform plan -var-file terraform.<stg|prod>.tfvars` to plan the
infrastructure.
4. Execute `terraform apply -var-file terraform.<stg|prod>.tfvars` to apply the
infrastructure.

### Infrastructure changes
1. Modify or add the new cloud resources.
2. Review the infrastructure with
`terraform plan -var-file terraform.<stg|prod>.tfvars`
3. Apply the changes `terraform apply -var-file terraform.<stg|prod>.tfvars`

### Destroy environment
Destruction of the resources can be performed running:
`terraform destroy -var-file terraform.<stg|prod>.tfvars`

### Using workspaces
The use of terraform workspaces can be used and it's recommended to have a
better management of the cloud resources, specially if only just one account
is being used.

## Possible improvements
With a proper configuration of aws profiles terraform operations can be
significantly simplified.

