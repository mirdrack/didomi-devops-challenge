# AWS
variable "aws_access_key" {}
variable "aws_secret_key" {}
variable "aws_region" { default = "us-east-2" }
variable "aws_availability_zones" {
  description = "The availability zones to deploy the resources"
  default     = ["us-east-2a", "us-east-2b"]
  type        = list(any)
}

# Environment
variable "environment_name" {}
variable "environment_type" {}

# Application
variable "app" { default = "didomi" }
variable "resource_tags" {
  description = "Tags to set for all resources"
  type        = map(string)
  default     = {}
}

# EC2
variable "ec2_key_name" {}

# Network
# These variables will configure the vpc and subnets in the infrastructure.
# Subnets were configured in a simple way just to avoid any overlap and
# proving enough space for the resources that will be deployed.
#
# For an enterprise environment subnets should be recalculated based on the
# needs of the project.
variable "vpc_cidr" {
  description = "CIDR for the whole VPC"
  default     = "10.0.0.0/16"
}
variable "public_subnets" {
  description = "The CIDR ranges to use in the public subnets"
  default     = ["10.0.16.0/24"]
  type        = list(any)
}
variable "private_subnets" {
  description = "The CIDR ranges to use in the private subnets"
  default     = ["10.0.1.0/24", "10.0.2.0/24"]
  type        = list(string)
}

# DNS
variable "domain_name" {}
variable "zone_id" {}

# Gitlab
variable "gitlab_token" {}
