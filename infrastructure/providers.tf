terraform {
  backend "s3" {
    bucket         = "terraform-states-storage"
    key            = "didomi-infrastructure/terraform.tfstate"
    region         = "us-west-2"
    dynamodb_table = "terraform-states-lock-dynamo"
  }

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
    random = {
      source  = "hashicorp/random"
      version = "~> 3.1.0"
    }
  }
  required_version = ">= 1.0.3"
}

provider "aws" {
  profile    = "default"
  region     = var.aws_region
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
}
