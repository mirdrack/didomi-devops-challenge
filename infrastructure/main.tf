# Added omments
locals {
  required_tags = {
    App            = var.app
    Environment    = var.environment_name
    CostAllocation = "${var.app}-${var.environment_type}"
    Build          = random_id.build.hex
    Terraform      = "true"
  }
  tags = merge(var.resource_tags, local.required_tags)
}

module "shared_network" {
  source           = "./modules/network"
  environment_type = var.environment_type
  name             = "${var.app}-${var.environment_type}-${random_id.build.hex}"
  cidr             = var.vpc_cidr
  azs              = var.aws_availability_zones
  public_subnets   = var.public_subnets
  private_subnets  = var.private_subnets
  tags             = local.tags
}

module "main_db" {
  source                = "./modules/database"
  app                   = var.app
  environment_name      = var.environment_name
  environment_type      = var.environment_type
  name                  = "${var.app}-${var.environment_type}-${random_id.build.hex}"
  engine                = "aurora-postgresql"
  engine_version        = "11.9"
  instance_type         = "db.t3.medium"
  instance_type_replica = "db.t3.medium"
  vpc_id                = module.shared_network.vpc_id
  allowed_cidr_blocks   = ["10.0.0.0/24"]
  subnets               = module.shared_network.private_subnets
  username              = "dbadmin"
  database_name         = "didomidb"
  tags                  = local.tags
}

module "main_certificate" {
  source      = "./modules/acm"
  domain_name = var.domain_name
  zone_id     = var.zone_id
  tags        = local.tags
}

module "gitlab_runners" {
  source = "./modules/gitlab-runner"

  # Autoscaling group
  name                      = "${var.app}-${var.environment_type}-${random_id.build.hex}"
  min_size                  = 1
  max_size                  = 3
  desired_capacity          = 1
  wait_for_capacity_timeout = 0
  vpc_zone_identifier       = module.shared_network.public_subnets

  # Launch template
  lt_name          = "${var.app}-${var.environment_type}-${random_id.build.hex}-lt"
  lt_description   = ""
  lt_image_id      = "ami-0443305dabd4be2bc" # parametrize
  lt_instance_type = "t3.micro"              # parametrize
  lt_placement_az  = module.shared_network.azs

  # Misc
  vpc_id       = module.shared_network.vpc_id
  key_name     = var.ec2_key_name
  gitlab_token = var.gitlab_token
  tags         = [local.tags]
}

module "lambda_iam" {
  source = "./modules/lambda"
  name = "${var.app}-${var.environment_type}-${random_id.build.hex}"
  environment_type = var.environment_type
  tags = local.tags
}

