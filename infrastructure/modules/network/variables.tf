variable "environment_type" {
  type = string
  description = "Type of evnironment"
}

variable "name" {
  type = string
  description = "Name for the vpc"
}

variable "cidr" {
  type = string
  description = "CIDR for the vpc"
}

variable "azs" {
  type = list
  description = "AWS availability zones"
}

variable "public_subnets" {
  type = list
  description = "List of the public sbunets"
}

variable "private_subnets" {
  type = list
  description = "List of the private sbunets"
}

variable "tags" {
  type = map
  description = "Tag list that will be include in all resources in the module"
}