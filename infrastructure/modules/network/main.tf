# Creates VPC resources on AWS
# https://github.com/terraform-aws-modules/terraform-aws-vpc
module "network" {
  source = "terraform-aws-modules/vpc/aws"
  version = "~> v3.2.0"

  name                = var.name
  cidr                = var.cidr
  azs                 = var.azs

  enable_dhcp_options = true
  enable_dns_hostnames = true

  # Gateway configuration
  enable_nat_gateway      = true
  reuse_nat_ips           = false
  single_nat_gateway      = true
  one_nat_gateway_per_az  = false

  # Subnets
  public_subnets      = var.public_subnets
  private_subnets     = var.private_subnets

  # Tags
  private_subnet_tags = merge(var.tags, tomap({"type" = "private"}))
  public_subnet_tags = merge(var.tags, tomap({"type" = "public"}))
  tags = var.tags
}

# Subnets will stored in parameter store to be used later
resource "aws_ssm_parameter" "private_subnets" {
  count = length(module.network.private_subnets)

  name        = "/${var.environment_type}/vpc/private_subnet_id_${count.index}"
  description = "Private subnet id ${count.index}"
  type        = "SecureString"
  value       = module.network.private_subnets[count.index]
  tags        = var.tags
}
