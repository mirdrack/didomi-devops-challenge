# Creates a root certificate that can be used with other cloud resources

# Since serverless domain manager only can handle certifcates on us-east-1
# region, this is to force the creation of the resources on this region.
provider "aws" {
  alias  = "us-east-1"
  region = "us-east-1"
}

module "acm" {
  source  = "terraform-aws-modules/acm/aws"
  version = "~> 3.0"

  providers = {
    aws = aws.us-east-1
  }

  domain_name  = var.domain_name
  zone_id      = var.zone_id

  subject_alternative_names = [
    "*.${var.domain_name}"
  ]

  wait_for_validation = true

  tags = var.tags
}
