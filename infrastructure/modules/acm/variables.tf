variable "domain_name" {
  type = string
  description = "Domain name that will be used for the certificate creations"
}

variable "zone_id" {
  type = string
  description = "AWS hosted zone that should be previously and created"
}

variable "tags" {
  type = map
  description = "Tag list that will be include in all resources in the module"
}
