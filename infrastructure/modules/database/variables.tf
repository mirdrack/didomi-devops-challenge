variable "environment_name" {}
variable "app" {}
variable "environment_type" {}

variable "name" {
  type = string
  description = "Name for cluster"
}

variable "engine" {
  description = "DB engine"
}

variable "engine_version" {
  description = "DB engine version"
}

variable "instance_type" {
  type = string
  description = "DB instance type"
}

variable "instance_type_replica" {
  type = string
  description = "DB instance type for replicas"
}

variable "vpc_id" {}
variable "allowed_cidr_blocks" {}
variable "subnets" {}

variable "username" {}
variable "password_length" {
  default = 16
  description = "Password will be randomly generated, this will define the lenght of it"
}
variable "database_name" {}

variable "tags" {
  type = map
  description = "Tag list that will be include in all resources in the module"
}
