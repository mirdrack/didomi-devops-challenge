module "db" {
  source  = "terraform-aws-modules/rds-aurora/aws"
  version = "~> 3.0"

  name           = var.name
  engine         = var.engine
  engine_version = var.engine_version
  instance_type  = var.instance_type
  instance_type_replica = var.instance_type_replica

  vpc_id                = var.vpc_id
  db_subnet_group_name  = aws_db_subnet_group.default.name
  subnets               = var.subnets
  create_security_group = true
  allowed_cidr_blocks   = var.allowed_cidr_blocks

  replica_count         = 1
  replica_scale_enabled = true
  replica_scale_min     = 1
  replica_scale_max     = 3

  apply_immediately   = true
  skip_final_snapshot = true

  db_parameter_group_name         = aws_db_parameter_group.main_db.id
  db_cluster_parameter_group_name = aws_rds_cluster_parameter_group.main_db.id

  username               = var.username
  create_random_password = false
  password = random_string.db_password.result

  database_name = var.database_name

  tags = var.tags
}

resource "aws_db_parameter_group" "main_db" {
  name_prefix = "${var.name}-aurora-db-postgres11-parameter-group"
  family      = "aurora-postgresql11"
  description = "${var.name}-aurora-db-postgres11-parameter-group"
  tags        = var.tags
}

resource "aws_rds_cluster_parameter_group" "main_db" {
  name_prefix = "${var.name}-aurora-postgres11-cluster-parameter-group"
  family      = "aurora-postgresql11"
  description = "${var.name}-aurora-postgres11-cluster-parameter-group"
  tags        = var.tags
}

resource "aws_db_subnet_group" "default" {
  name       = "${var.app}-${var.environment_type}-subnet-group"
  subnet_ids = var.subnets
  tags = var.tags
}

resource "random_string" "db_password" {
  length  = var.password_length
  special = false
}

# Once the cluster was generated the needed values to establish a connection
# will be stored in the parameter store to be queried by the application.
resource "aws_ssm_parameter" "db_password" {
  name        = "/${var.environment_type}/db/password"
  description = "Password to connect to the ${var.name} cluster"
  type        = "SecureString"
  value       = module.db.this_rds_cluster_master_password
  tags        = var.tags
}

resource "aws_ssm_parameter" "rds_cluster_endpoint" {
  name        = "/${var.environment_type}/db/rds_cluster_endpoint"
  description = "Endpoint to connect to the ${var.name} cluster"
  type        = "SecureString"
  value       = module.db.this_rds_cluster_endpoint
  tags        = var.tags
}

resource "aws_ssm_parameter" "username" {
  name        = "/${var.environment_type}/db/username"
  description = "Username to connect to the ${var.name} cluster"
  type        = "SecureString"
  value       = module.db.this_rds_cluster_master_username
  tags        = var.tags
}

resource "aws_ssm_parameter" "database_name" {
  name        = "/${var.environment_type}/db/database"
  description = "Deafult database in ${var.name} cluster"
  type        = "SecureString"
  value       = var.database_name
  tags        = var.tags
}

resource "aws_ssm_parameter" "security_group_id" {
  name        = "/${var.environment_type}/db/security_group_id"
  description = "Deafult security group id for ${var.name} cluster"
  type        = "SecureString"
  value       = module.db.this_security_group_id
  tags        = var.tags
}
