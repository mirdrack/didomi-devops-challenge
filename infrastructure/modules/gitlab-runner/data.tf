data "template_file" "init" {
  template = templatefile("${path.module}/init.tpl", {token = var.gitlab_token})
}