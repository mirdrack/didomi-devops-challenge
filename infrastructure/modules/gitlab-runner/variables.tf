# Autoscaling group
variable "name" {}
variable "min_size" {}
variable "max_size" {}
variable "desired_capacity" {}
variable "wait_for_capacity_timeout" {}
variable "health_check_type" {
  default = "EC2"
}
variable "vpc_zone_identifier" {
  type = list
  description = "List of sbunets to place the instances"
}

# Launch template
variable "lt_name" {}
variable "lt_description" {}
variable "lt_image_id" {}
variable "lt_instance_type" {}
variable "lt_security_groups" {
  default = []
}
variable "lt_placement_az" {}

# Misc
variable "vpc_id" {}
variable "key_name" {}
variable "gitlab_token" {}
variable "tags" {
  type = map
  description = "Tag list that will be include in all resources in the module"
}
