#!/bin/bash

# This script will be used as user data for the ec2 instances and to register
# the manchines as Gitlab runners.
apt install git docker -y
curl -s https://packages.gitlab.com/install/repositories/runner/gitlab-ci-multi-runner/script.rpm.sh | sudo bash

curl --request POST "https://gitlab.com/api/v4/runners" \
     --form "token=${token}" --form "description=GL Runner" \
     --form "tag_list=node"
