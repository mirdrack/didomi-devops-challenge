resource "aws_launch_configuration" "gitlab_runner" {
  name                        = var.name
  image_id                    = var.lt_image_id
  instance_type               = var.lt_instance_type
  user_data                   = data.template_file.init.rendered
  iam_instance_profile        = aws_iam_instance_profile.gitlab_runner.arn
  associate_public_ip_address = true
  key_name                    = var.key_name
  security_groups             = var.lt_security_groups

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "gitlab_runner" {
  min_size             = var.min_size
  max_size             = var.max_size
  desired_capacity     = var.desired_capacity
  launch_configuration = aws_launch_configuration.gitlab_runner.name
  vpc_zone_identifier  = var.vpc_zone_identifier

  lifecycle {
    create_before_destroy = true
  }

  tags = var.tags
}

resource "aws_iam_instance_profile" "gitlab_runner" {
  name = "${var.name}_profile"
  role = aws_iam_role.gitlab_runner.name
}

resource "aws_iam_role" "gitlab_runner" {
  name = "${var.name}_role"
  path = "/"

  # The policy could be stored in a json or a templated file but was placed
  # here to provide more visibilty about the configuration.
  assume_role_policy = <<-EOF
  {
      "Version": "2012-10-17",
      "Statement": [
          {
              "Action": "sts:AssumeRole",
              "Principal": {
                 "Service": "ec2.amazonaws.com"
              },
              "Effect": "Allow",
              "Sid": ""
          }
      ]
  }
EOF
}

# The policy could be stored in a json or a templated file but was placed
# here to provide more visibilty about the configuration.
# It has to many permisons open but this can be adjusted according to the needs
# of the project
resource "aws_iam_policy" "gitlab_runner" {
  name        = "${var.name}_policy"
  path        = "/"
  description = "Gitlab runner IAM policy"
  policy      = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "ssm:*",
        "dynamodb:*",
        "kms:*",
        "sns:*",
        "sqs:*",
        "cloudwatch:*",
        "apigateway:*",
        "cloudformation:*",
        "iam:getrole",
        "iam:createrole",
        "iam:updaterole",
        "iam:AttachRolePolicy",
        "iam:UpdateAssumeRolePolicy",
        "iam:CreateServiceLinkedRole",
        "iam:CreateInstanceProfile",
        "iam:DeleteInstanceProfile",
        "iam:AddRoleToInstanceProfile",
        "iam:RemoveRoleFromInstanceProfile",
        "iam:ListInstanceProfilesForRole",
        "iam:PutRolePolicy",
        "iam:DetachRolePolicy",
        "iam:DeleteRolePolicy",
        "iam:DeleteRole",
        "iam:PassRole",
        "iam:get*",
        "logs:*",
        "lambda:*",
        "s3:*",
        "ec2:*",
        "rds:*",
        "route53:Get*",
        "route53:ChangeResourceRecordSets",
        "route53:List*",
        "autoscaling:*"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_iam_policy_attachment" "gitlab_runner" {
  name       = "${var.name}_gitlab_runner_attachment"
  roles      = ["${aws_iam_role.gitlab_runner.name}"]
  policy_arn = "${aws_iam_policy.gitlab_runner.arn}"
}

resource "aws_security_group" "gitlab_runner" {
  vpc_id      = var.vpc_id
  name        = "${var.name}_sg"
  description = "Allow traffic to pass"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    description = ""
  }
  ingress {
    from_port   = 2376
    to_port     = 2376
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    description = ""
  }
  ingress {
    from_port   = 3376
    to_port     = 3376
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    description = ""
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    description = ""
  }
}