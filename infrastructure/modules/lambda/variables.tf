variable "name" {
  type = string
  description = "Name that will be used to associate the IAM resources"
}

variable "environment_type" {
  type = string
  description = "The type of environment is being created"
}

variable "tags" {
  type = map
  description = "Tag list that will be include in all resources in the module"
}