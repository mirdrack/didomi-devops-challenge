# This module will create an aws role with a custom policy for lambda based on
# the tasks the lambda function will perform.
# This module is based on raw aws resources.

resource "aws_iam_role" "lambda" {
  name = "${var.name}_lambda_role"
  path = "/"

  # The policy could be stored in a json or a templated file but was placed
  # here to provide more visibilty about the configuration.
  assume_role_policy = <<-EOF
  {
    "Version": "2012-10-17",
    "Statement": [
      {
        "Effect": "Allow",
        "Principal": {
          "Service": "lambda.amazonaws.com"
        },
        "Action": "sts:AssumeRole"
      }
    ]
  }
  EOF
}

# The permisions on this policy are too open and should be updated once the
# details of the operations are defined.
# Cloudtrail could help to review the operations are being performed and keep
# the permissions close as possible.
# Again policy was placed here to offer better visibility.
resource "aws_iam_policy" "lambda" {
  name        = "${var.name}_lambda_policy"
  path        = "/"
  description = "Gitlab runner IAM policy"
  policy      = <<-EOF
  {
    "Version": "2012-10-17",
    "Statement": [
      {
        "Action": [
          "s3:GetObject"
        ],
        "Effect": "Allow",
        "Resource": "*"
      },
      {
        "Action": [
          "cloudwatch:CreateLogStream",
          "cloudwatch:CreateLogGroup",
          "cloudwatch:PutLogEvents",
          "cloudwatch:CreateLogGroup",
          "cloudwatch:CreateLogStream",
          "cloudwatch:PutLogEvent"
        ],
        "Effect": "Allow",
        "Resource": "*"
      },
      {
        "Action": [
          "ec2:CreateNetworkInterface",
          "ec2:DescribeNetworkInterfaces",
          "ec2:DeleteNetworkInterface",
          "ec2:AssignPrivateIpAddresses",
          "ec2:UnassignPrivateIpAddresses"
        ],
        "Effect": "Allow",
        "Resource": "*"
      }
    ]
  }
  EOF
}

resource "aws_iam_policy_attachment" "lambda" {
  name       = "${var.name}_lambda_attachment"
  roles      = ["${aws_iam_role.lambda.name}"]
  policy_arn = aws_iam_policy.lambda.arn
}

resource "aws_ssm_parameter" "lambda_role_arn" {
  name        = "/${var.environment_type}/lambda/role_arn"
  description = "Role for the lambdas"
  type        = "SecureString"
  value       = aws_iam_role.lambda.arn
  tags        = var.tags
}
