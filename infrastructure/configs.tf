resource "random_id" "build" {
  byte_length = 4
}

resource "aws_ssm_parameter" "domain_name" {
  name        = "/${var.environment_type}/dns/domain_name"
  description = "Custom domain that will be as root domain"
  type        = "SecureString"
  value       = var.domain_name
  tags        = local.tags
}